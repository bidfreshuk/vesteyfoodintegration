'**********************************************************************************************************
' 1. Create a Console app
' 2. Rename Module1.vb to Main.vb or what ever.
' 3. add a project reference to Conduit.ConsoleBase
' 4. Paste the following into Main.vb
' 5. Set up the correct settings in your app.config
'       LogFilePath, RemoteHost, NotificationEmail, NotifyOnSuccess and NotificationSubject
' 5. you can use _log and _email
'
'Imports Conduit.ConsoleBase
'
'Public Class Main
'    Inherits MainBase
'
'    Public Shared Function main() As Integer
'        Return InitAndTryCatchWrapMainProcess(AddressOf MainProcess)
'    End Function
'
'
'    Protected Shared Sub MainProcess()
'        'Do your thing here
'    End Sub
'
'End Class
'**********************************************************************************************************

Imports System
Imports System.Configuration
Imports Conduit.ConsoleBase


Public Class AppGlobals
    Private Shared _testing As Boolean = False
    Private Shared _debug As Boolean = False

    Public Shared Property TestMode As Boolean
        Get
            Return _testing
        End Get
        Set(ByVal value As Boolean)
            _testing = value
        End Set
    End Property

    Public Shared Property DebugMode As Boolean
        Get
            Return _debug
        End Get
        Set(ByVal value As Boolean)
            _debug = value
        End Set
    End Property

End Class

Public MustInherit Class MainBase
    Protected Shared _log As Log
    Protected Shared _email As Email



    Public Delegate Sub DoMainProcess()

    Public Shared Function InitAndTryCatchWrapMainProcess(ByVal doMainProcess As DoMainProcess) As Integer
        Try
            Dim args() As String = Environment.GetCommandLineArgs()

            InitialEmailAndLog()

            If args.Length = 2 AndAlso args(1).ToLower() = "test" Then
                _email.SendMessage("this is a test", "this is a test")
                Return 0
            End If

            doMainProcess()

            _log.Write("Finished")
            _log.Write("---------------------------------------------------")
            Return 0

        Catch ex As Exception
            Try
                _log.Write(ex.ToString())
            Catch ex2 As Exception
                Console.Error.WriteLine(ex.ToString() + ex2.ToString())
            End Try

            Try
                _email.SendError(ex, _log)
            Catch ex2 As Exception
                Console.Error.WriteLine(ex.ToString() + ex2.ToString())
            End Try

            Return 1
        Finally
            If Not _log Is Nothing Then
                _log.Close()
            End If
        End Try
    End Function

    Public Shared ReadOnly Property mailToNotificationAddress As String
        Get
            Return _email.NotificationToEmailAddress
        End Get
    End Property


    Private Shared ReadOnly Property DateFileSuffix() As String
        Get
            Return DateTime.Now.ToString("yyyyMMdd") & ".log"
        End Get
    End Property

    Protected Shared Function GetExpectedAppSetting(ByVal appSettingKey As String) As String
        If String.IsNullOrEmpty(ConfigurationManager.AppSettings(appSettingKey)) Then
            Throw New ApplicationException("Could not find app setting value: '" & appSettingKey & "' in config file.")
        End If

        Return ConfigurationManager.AppSettings(appSettingKey)
    End Function

    Protected Shared Function GetAppSetting(ByVal appSettingKey As String, ByVal defatulValue As Object) As Object
        If String.IsNullOrEmpty(ConfigurationManager.AppSettings(appSettingKey)) Then
            Return defatulValue
        End If

        Return ConfigurationManager.AppSettings(appSettingKey)
    End Function


    Protected Shared Sub InitialEmailAndLog()
        _log = New Log(GetExpectedAppSetting("LogFilePath") & DateFileSuffix)
        SetEmailSettings()
    End Sub

    Private Shared Sub SetEmailSettings()
        _email = New Email()
        _email.RemoteHost = GetExpectedAppSetting("RemoteHost")
        _email.Port = Convert.ToInt32(GetAppSetting("EmailPort", 25))
        _email.NotificationToEmailAddress = GetExpectedAppSetting("NotificationEmail")
        _email.NotificationFromEmailAddress = GetAppSetting("NotificationFromEmailAddress", "SiteAudit@bluepepper.co.nz").ToString()
        _email.NotifyOnSuccess = Convert.ToBoolean(GetExpectedAppSetting("NotifyOnSuccess"))
        _email.NotificationSubject = GetExpectedAppSetting("NotificationSubject")

        'the following lines are not used by any codes
        'Boolean.TryParse(GetExpectedAppSetting("TestMode"), AppGlobals.TestMode)
        'Boolean.TryParse(GetExpectedAppSetting("DebugMode"), AppGlobals.DebugMode)
     End Sub
End Class
