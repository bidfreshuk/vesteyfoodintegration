Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System
Imports System.Reflection

Public Class SimpleDataReader
	Implements IDisposable


	Private m_reader As DbDataReader

	Public ReadOnly Property IsClosed() As Boolean
        Get
            Return m_reader.IsClosed
        End Get
	End Property

	Public ReadOnly Property HasRows() As Boolean
		Get
			Return m_reader.HasRows
		End Get
	End Property

	Public ReadOnly Property FieldCount() As Integer
		Get
			Return m_reader.FieldCount
		End Get
	End Property

	Public ReadOnly Property VisibleFieldCount() As Integer
		Get
			Return m_reader.VisibleFieldCount
		End Get
	End Property

	Public Sub New(ByVal reader As DbDataReader)
		m_reader = reader
	End Sub

	Public Function Read() As Boolean
		Return m_reader.Read()
	End Function

    Public Function GetValue(Of T)(ByVal name As String) As T
        Try
            Dim ordinal As Integer = m_reader.GetOrdinal(name)
            If m_reader.IsDBNull(ordinal) Then
                Return Nothing
            Else
                Return CType(m_reader.GetValue(ordinal), T)
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function GetValue(Of T)(ByVal ordinal As Integer) As T
        Try
            If m_reader.IsDBNull(ordinal) Then
                Return Nothing
            Else
                Return CType(m_reader.GetValue(ordinal), T)
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function GetValue(Of T)(ByVal name As String, ByVal nullValue As T) As T
        Try
            Dim ordinal As Integer = m_reader.GetOrdinal(name)
            If m_reader.IsDBNull(ordinal) Then
                Return nullValue
            Else
                Return CType(m_reader.GetValue(ordinal), T)
            End If
        Catch ex As Exception
            Return nullValue
        End Try
    End Function

    Public Function GetValue(Of T)(ByVal ordinal As Integer, ByVal nullValue As T) As T
        Try
            If m_reader.IsDBNull(ordinal) Then
                Return nullValue
            Else
                Return CType(m_reader.GetValue(ordinal), T)
            End If
        Catch ex As Exception
            Return nullValue
        End Try
    End Function

	Public Function IsNull(ByVal name As String) As Boolean
		Return m_reader.IsDBNull(m_reader.GetOrdinal(name))
	End Function

	Public Function IsNull(ByVal ordinal As Integer) As Boolean
		Return m_reader.IsDBNull(ordinal)
	End Function

	Public Function GetString(ByVal name As String) As String
		Return Me.GetValue(Of String)(name)
	End Function

	Public Function GetString(ByVal name As String, ByVal nullValue As String) As String
		Return Me.GetValue(Of String)(name, nullValue)
	End Function

	Public Function GetInteger(ByVal name As String) As Integer
		Return Me.GetValue(Of Integer)(name)
    End Function
    

	Public Function GetInteger(ByVal name As String, ByVal nullValue As Integer) As Integer
		Return Me.GetValue(Of Integer)(name, nullValue)
	End Function

	Public Function GetDouble(ByVal name As String) As Double
		Return Me.GetValue(Of Double)(name)
	End Function

	Public Function GetDouble(ByVal name As String, ByVal nullValue As Double) As Double
		Return Me.GetValue(Of Double)(name, nullValue)
	End Function

	Public Function GetBoolean(ByVal name As String) As Boolean
		Return Me.GetValue(Of Boolean)(name)
	End Function

	Public Function GetBoolean(ByVal name As String, ByVal nullValue As Boolean) As Boolean
		Return Me.GetValue(Of Boolean)(name, nullValue)
	End Function

	Public Sub Close()
		If Not m_reader.IsClosed Then
			m_reader.Close()
		End If
	End Sub

    Public Function HasColumn(ByVal columnName As String) As Boolean
        Dim dr As IDataRecord = m_reader
        For i As Integer = 0 To dr.FieldCount - 1
            If dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase) Then
                Return True
            End If
        Next
        Return False
    End Function

    ' ------------------------------------------------------------------------------
    ' To use this function, the generic type must be same as the query result type.
    ' ------------------------------------------------------------------------------
    Public Function ConvertToList(Of T As {New})() As List(Of T)
        Dim list As List(Of T) = New List(Of T)()
        Dim index As Integer = 0
        Dim columnName As String = String.Empty

        If IsNothing(m_reader) = False Then
            While m_reader.Read
                Dim genObj As T = New T()
                For index = 0 To m_reader.FieldCount - 1
                    columnName = m_reader.GetName(index)
                    Dim genClassType As Type = genObj.GetType()
                    Dim genTypePropertyInfo As PropertyInfo = genClassType.GetProperty(columnName)
                    genTypePropertyInfo.SetValue(genObj, m_reader.GetValue(index), Nothing)
                Next
                list.Add(genObj)
            End While
        Else
            list = Nothing
        End If

        Return list
    End Function


    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If Not m_reader.IsClosed Then
                    m_reader.Close()
                End If
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
