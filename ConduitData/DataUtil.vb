Public Class DataUtil

	Public Shared Function FilterDBNull(Of T)(ByVal value As Object) As T
		If value.Equals(System.DBNull.Value) Then
			Return Nothing
		End If
		Return CType(value, T)
	End Function

	Public Shared Function IfDBNull(Of T)(ByVal value As Object, ByVal replacement As T) As T
		If value.Equals(System.DBNull.Value) Then
			Return replacement
		End If
		Return CType(value, T)
	End Function

	Public Shared Function IsDBNull(ByVal value As Object) As Boolean
		Return value.Equals(System.DBNull.Value)
	End Function

End Class
