Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Configuration

<Serializable()> Public Class StoredProcedure
    Implements IDisposable
    Private m_connection As SqlConnection
    Private m_cmd As SqlCommand

    Public ReadOnly Property Command() As SqlCommand
        Get
            Return m_cmd
        End Get
    End Property

    Public Property Name() As String
        Get
            Return m_cmd.CommandText
        End Get
        Set(ByVal value As String)
            m_cmd.CommandText = value
        End Set
    End Property

    Public Sub New(ByVal name As String, ByVal connectionStringName As String, ByVal connectionString As String)
        If String.IsNullOrEmpty(connectionStringName) Then
            SetCommand(name, New SqlConnection(connectionString))
        Else
            SetCommand(name, New SqlConnection(Me.GetConnectionStringFromWebConfig(connectionStringName)))
        End If
    End Sub

    Public Sub New(ByVal name As String, ByVal connection As SqlConnection)
        SetCommand(name, connection)
    End Sub

    Public Sub New(ByVal name As String, ByVal connectionStringName As String)
        If String.IsNullOrEmpty(connectionStringName) Then
            SetCommand(name, New SqlConnection(Me.GetConnectionStringFromWebConfig()))
        Else
            SetCommand(name, New SqlConnection(Me.GetConnectionStringFromWebConfig(connectionStringName)))
        End If
    End Sub

    Public Sub New(ByVal name As String)
        SetCommand(name, New SqlConnection(Me.GetConnectionStringFromWebConfig()))
    End Sub

    Private _transaction As SqlTransaction
    Public Sub BeginTransaction()
        OpenIfClosed()
        _transaction = m_connection.BeginTransaction()
        m_cmd.Transaction = _transaction
    End Sub

    Public Sub CommitTransaction()
        _transaction.Commit()
        _transaction = Nothing
    End Sub

    Public Sub RollbackTransaction()
        _transaction.Rollback()
        _transaction = Nothing
    End Sub

    Private Sub SetCommand(ByVal procName As String, ByVal connection As SqlConnection)
        m_connection = connection
        m_cmd = New SqlCommand(procName, m_connection)
        m_cmd.CommandType = CommandType.StoredProcedure
        SetCommandTimeout()
    End Sub

    Private Sub SetCommandTimeout()

        Dim commandTimeOutFromConfig As Integer = 0

        Dim CommandTimeoutDefault As Integer = m_cmd.CommandTimeout
        If m_cmd.Connection.ConnectionTimeout > CommandTimeoutDefault Then
            CommandTimeoutDefault = m_cmd.Connection.ConnectionTimeout
        End If

        commandTimeOutFromConfig = Convert.ToInt32(GetAppSetting("CommandTimeOut", CommandTimeoutDefault))


        m_cmd.CommandTimeout = commandTimeOutFromConfig
    End Sub

    Private Function GetConnectionStringFromWebConfig() As String
        Static Dim retVal As String = Nothing
        If retVal = Nothing Then
            Dim i As Integer = CType(IIf(System.Configuration.ConfigurationManager.ConnectionStrings.Count > 1, 1, 0), Integer)
            Dim data As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings(i)
            If Not data Is Nothing Then
                retVal = data.ConnectionString
            End If
        End If
        Return retVal
    End Function

    Private Function GetConnectionStringFromWebConfig(ByVal name As String) As String
        Dim data As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings(name)
        If Not data Is Nothing Then
            Return data.ConnectionString
        End If
        Return Nothing
    End Function

    Public Sub AddParameter(Of T)(ByVal name As String, ByVal value As T, ByVal type As SqlDbType)
        Dim param As New SqlParameter(name, type)

        If value Is Nothing Then
            param.Value = DBNull.Value
        Else
            param.Value = value
        End If

        param.Direction = ParameterDirection.Input
        m_cmd.Parameters.Add(param)
    End Sub

    Public Sub AddParameter(Of T)(ByVal name As String, ByVal value As T, ByVal type As SqlDbType, ByVal size As Integer)
        Dim param As New SqlParameter(name, type)
        param.Size = size
        param.Direction = ParameterDirection.Input
        param.Value = value
        m_cmd.Parameters.Add(param)
    End Sub

    Public Sub AddParameter(Of T)(ByVal paramName As String, ByVal value As T)
        If value Is Nothing Then
            m_cmd.Parameters.AddWithValue(paramName, DBNull.Value)
        Else
            AddParameter(Of T)(paramName, value, GetSqlDBType(value.GetType()))
        End If
    End Sub

    Public Sub AddParameterSimple(ByVal paramName As String, ByVal value As Object)
        If value Is Nothing Then
            m_cmd.Parameters.AddWithValue(paramName, DBNull.Value)
        Else
            m_cmd.Parameters.AddWithValue(paramName, value)
        End If
    End Sub

    Public Sub AddParamter(param As SqlParameter)
        m_cmd.Parameters.Add(param)
    End Sub

    'Private Function GetSqlDbType(Of T)(ByVal value As T) As SqlDbType
    '    'Should really get this info from T rather than value. But I could not get it to work with out wasting too much time. If you can come and tell me how you did it. Rob.
    '    If TypeOf value Is Integer Then
    '        Return SqlDbType.Int
    '    ElseIf TypeOf value Is String Then
    '        Return SqlDbType.NVarChar
    '    ElseIf TypeOf value Is Decimal Then
    '        Return SqlDbType.Decimal
    '    ElseIf TypeOf value Is Boolean Then
    '        Return SqlDbType.Bit
    '    Else
    '        Throw New ApplicationException("I have not catered for this type. You can add it here (or call the AddParameter overload specifying the type you want).")
    '    End If
    'End Function

    Private Function GetSqlDBType(ByVal theType As System.Type) As SqlDbType
        Dim p1 As SqlClient.SqlParameter
        Dim tc As System.ComponentModel.TypeConverter
        p1 = New SqlClient.SqlParameter()
        tc = System.ComponentModel.TypeDescriptor.GetConverter(p1.DbType)
        If tc.CanConvertFrom(theType) Then
            p1.DbType = CType(tc.ConvertFrom(theType.Name), DbType)
        Else
            'Try brute force
            Try
                p1.DbType = CType(tc.ConvertFrom(theType.Name), DbType)
            Catch ex As Exception
                'Do Nothing
            End Try
        End If
        Return p1.SqlDbType
    End Function


    Public Sub AddOutputParameter(ByVal name As String, ByVal type As SqlDbType, ByVal size As Integer)
        Dim param As New SqlParameter(name, type, size)
        param.Direction = ParameterDirection.Output
        m_cmd.Parameters.Add(param)
    End Sub

    Public Sub AddOutputParameter(ByVal name As String, ByVal type As SqlDbType)
        Dim param As New SqlParameter(name, type)
        param.Direction = ParameterDirection.Output
        m_cmd.Parameters.Add(param)
    End Sub

    Public Function GetOutputParameter(Of T)(ByVal name As String) As T
        Return CType(m_cmd.Parameters(name).Value, T)
    End Function

    Public Sub AddReturnValue(ByVal type As SqlDbType)
        Dim param As New SqlParameter("@return_value", type)
        param.Direction = ParameterDirection.ReturnValue
        m_cmd.Parameters.Add(param)
    End Sub

    Public Sub AddReturnValue()
        Dim param As New SqlParameter("@return_value", SqlDbType.Int)
        param.Direction = ParameterDirection.ReturnValue
        m_cmd.Parameters.Add(param)
    End Sub

    Public Function GetReturnValue(Of T)() As T
        Return CType(m_cmd.Parameters("@return_value").Value, T)
    End Function

    Public Function GetReturnValue() As Integer
        Return CInt(m_cmd.Parameters("@return_value").Value)
    End Function


    Public Function ExecuteNonQuery() As Integer
        Try
            OpenIfClosed()
            Return m_cmd.ExecuteNonQuery()      ' rows affected 
        Catch ex As SqlException
            Throw ex
        Finally
            If _transaction Is Nothing Then
                CloseConnection()
            End If
        End Try
    End Function

    Public Function ExecuteScalar(Of T)() As T
        Try
            OpenIfClosed()
            Return CType(m_cmd.ExecuteScalar(), T)
        Catch ex As Exception
            Throw ex
        Finally
            If _transaction Is Nothing Then
                CloseConnection()
            End If
        End Try
    End Function

    Public Function ExecuteReader(ByVal behavior As CommandBehavior) As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        Try
            OpenIfClosed()
            reader = m_cmd.ExecuteReader(behavior)
        Catch
            If Not reader Is Nothing Then
                If Not reader.IsClosed Then reader.Close()
            End If
            Throw
        End Try
        Return reader
    End Function

    Public Function ExecuteReader() As SqlDataReader
        Return Me.ExecuteReader(CommandBehavior.CloseConnection)
    End Function

    Public Function ExecuteSimpleDataReader() As Conduit.Data.SimpleDataReader
        Dim reader As SqlDataReader = Nothing
        Try
            OpenIfClosed()
            reader = m_cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Return New Conduit.Data.SimpleDataReader(reader)
        Catch Ex As Exception
            If Not reader Is Nothing Then
                If Not reader.IsClosed Then reader.Close()
            End If
            Throw Ex
        End Try
        Return Nothing
    End Function

    Public Function ExecuteDataSet(ByVal name As String) As DataSet
        Dim dataSet As New DataSet(name)
        Dim dataAdapter As New SqlDataAdapter(m_cmd)
        OpenIfClosed()
        Try
            dataAdapter.Fill(dataSet)
            Return dataSet
        Finally
            If _transaction Is Nothing Then
                CloseConnection()
            End If
        End Try
    End Function

    Private Sub OpenIfClosed()
        If m_connection.State = ConnectionState.Closed Then
            m_connection.Open()
        End If
    End Sub

    Public Function ExecuteDataSet() As DataSet
        Dim dataSet As New DataSet()
        Try
            Dim dataAdapter As New SqlDataAdapter(m_cmd)
            OpenIfClosed()
            dataAdapter.Fill(dataSet)
            Return dataSet
        Finally
            If _transaction Is Nothing Then
                CloseConnection()
            End If
        End Try
    End Function

    Public Function ExecuteDataTable() As DataTable
        Try
            Dim results As DataSet = ExecuteDataSet()

            If results.Tables.Count = 0 Then
                Throw New ApplicationException("No results from stored procedure")
            End If

            Return results.Tables(0)
        Finally
            If _transaction Is Nothing Then
                CloseConnection()
            End If
        End Try
    End Function

    Public Sub ClearParameters()
        m_cmd.Parameters.Clear()
    End Sub

    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder
        sb.Append(m_cmd.CommandText)
        If m_cmd.Parameters.Count > 0 Then
            For Each param As SqlParameter In m_cmd.Parameters
                sb.Append(" ")
                Dim pValue As String
                If param.SqlValue Is Nothing Then
                    pValue = "null"
                Else
                    pValue = param.SqlValue.ToString
                End If
                Select Case param.SqlDbType
                    Case SqlDbType.Char, SqlDbType.NChar, SqlDbType.NText, SqlDbType.NVarChar, _
                      SqlDbType.Text, SqlDbType.UniqueIdentifier, SqlDbType.VarChar, SqlDbType.Xml                        
                        sb.AppendFormat("'{0}'", pValue)
                    Case SqlDbType.Binary, SqlDbType.Image, SqlDbType.VarBinary
                        sb.Append("[BINARY]")
                    Case Else
                        sb.Append(pValue)
                End Select
                sb.Append(",")
            Next
            sb.Remove(sb.Length - 1, 1)
        End If
        Return sb.ToString()
    End Function

    Public Sub CloseConnection()
        If m_connection IsNot Nothing Then
            If m_connection.State <> ConnectionState.Closed Then
                m_connection.Close()
            End If
        End If
    End Sub


    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                CloseConnection()
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

    Protected Shared Function GetAppSetting(ByVal appSettingKey As String, ByVal defatulValue As Object) As Object
        If String.IsNullOrEmpty(ConfigurationManager.AppSettings(appSettingKey)) Then
            Return defatulValue
        End If

        Return ConfigurationManager.AppSettings(appSettingKey)
    End Function


#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    

End Class