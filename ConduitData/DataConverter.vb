Imports System.Data
Imports System.Data.Common
Imports System.Text
Imports System.IO
Imports Microsoft.VisualBasic

Public Class DataConverter
    Implements IDisposable

    Private _ds As Object
    Private _isDataSet As Boolean

    Public Sub New(ByVal dataSource As Object)
        If TypeOf dataSource Is DataSet Then
            _isDataSet = True
            _ds = dataSource
        ElseIf TypeOf dataSource Is DbDataReader Then
            _isDataSet = False
            _ds = dataSource
        Else
            Throw New ArgumentException("Expected System.Data.DataSet or System.Data.Common.DbDataReader", "dataSource")
        End If
    End Sub

    Public Function ToCSV(ByVal includeHeaderRow As Boolean) As String
        If _isDataSet Then
            Return DataConverter.ToCSV(CType(_ds, DataSet), includeHeaderRow)
        Else
            Return DataConverter.ToCSV(CType(_ds, DbDataReader), includeHeaderRow)
        End If
    End Function

    Public Sub ToCSV(ByVal s As Stream, ByVal includeHeaderRow As Boolean)
        If _isDataSet Then
            DataConverter.ToCSV(s, CType(_ds, DataSet), includeHeaderRow)
        Else
            DataConverter.ToCSV(s, CType(_ds, DbDataReader), includeHeaderRow)
        End If
    End Sub

    Public Shared Function ToCSV(ByVal reader As DbDataReader, ByVal includeHeaderRow As Boolean) As String
        ' Make sure we have data
        If reader Is Nothing OrElse Not reader.HasRows Then
            ' Return an empty string
            Return String.Empty
        End If

        Dim sb As New StringBuilder()

        ' Read in first record
        reader.Read()

        Dim fieldCount As Integer = reader.FieldCount

        ' If we want a header
        If includeHeaderRow Then
            ' Iterate through fields in first row, adding their names to the output
            For i As Integer = 0 To fieldCount - 1
                ' If it's not the first field, add a comma to the output
                If i > 0 Then sb.Append(",")
                ' Get the field name
                Dim fieldName As String = reader.GetName(i)
                ' If there's a comma in the field name
                If fieldName.IndexOf(",") > -1 Then
                    ' Add the field name enclosed in quotes, with any internal quotes escaped
                    sb.AppendFormat("""{0}""", fieldName.Replace("""", """"""))
                Else
                    ' Otherwise, just add the field name
                    sb.Append(fieldName)
                End If
            Next
            sb.AppendLine()
        End If

        ' Iterate through the data, adding the field values to the output
        Do
            ' Iterate through the fields in the current row
            For i As Integer = 0 To fieldCount - 1
                ' If it's not the first field, add a comma to the output
                If i > 0 Then sb.Append(",")
                ' If the field's value is not null (we have a value)
                If Not reader.IsDBNull(i) Then
                    Dim fieldValue As String = reader.GetValue(i).ToString().Replace(vbCrLf, vbTab)
                    ' If there's a comma in the field value
                    If fieldValue.IndexOf(",") > -1 Then
                        ' Add the field value enclosed in quotes, with any internal quotes escaped
                        sb.AppendFormat("""{0}""", fieldValue.Replace("""", """"""))
                    Else
                        ' Otherwise, just add the field value
                        sb.Append(fieldValue)
                    End If
                End If
            Next
            sb.AppendLine()
        Loop While reader.Read()

        ' Return the csv data
        Return sb.ToString()
    End Function

    Public Shared Function ToCSV(ByVal ds As DataSet, ByVal includeHeaderRow As Boolean) As String
        ' Make sure we have data
        If ds Is Nothing OrElse Not ds.Tables.Count > 0 OrElse Not ds.Tables(0).Rows.Count > 0 Then
            ' Return an empty string
            Return String.Empty
        End If

        Dim sb As New StringBuilder()

        ' Get first table
        Dim table As DataTable = ds.Tables(0)

        ' Get field count
        Dim fieldCount As Integer = table.Columns.Count

        ' If we want a header
        If includeHeaderRow Then
            ' Iterate through fields in first row, adding their names to the output
            For i As Integer = 0 To fieldCount - 1
                ' If it's not the first field, add a comma to the output
                If i > 0 Then sb.Append(",")
                ' Get the field name
                Dim fieldName As String = table.Columns(i).ColumnName
                ' If there's a comma in the field name
                If fieldName.IndexOf(",") > -1 Then
                    ' Add the field name enclosed in quotes, with any internal quotes escaped
                    sb.AppendFormat("""{0}""", fieldName.Replace("""", """"""))
                Else
                    ' Otherwise, just add the field name
                    sb.Append(fieldName)
                End If
            Next
            sb.AppendLine()
        End If

        ' Iterate through the data, adding the field values to the output
        For Each row As DataRow In table.Rows
            ' Iterate through the fields in the current row
            For i As Integer = 0 To fieldCount - 1
                ' If it's not the first field, add a comma to the output
                If i > 0 Then sb.Append(",")
                ' If the field's value is not null (we have a value)
                If Not row.IsNull(i) Then
                    Dim fieldValue As String = row.Item(i).ToString().Replace(vbCrLf, vbTab)
                    ' If there's a comma in the field value
                    If fieldValue.IndexOf(",") > -1 Then
                        ' Add the field value enclosed in quotes, with any internal quotes escaped
                        sb.AppendFormat("""{0}""", fieldValue.Replace("""", """"""))
                    Else
                        ' Otherwise, just add the field value
                        sb.Append(fieldValue)
                    End If
                End If
            Next
            sb.AppendLine()
        Next

        ' Return the csv data
        Return sb.ToString()
    End Function

    Public Shared Sub ToCSV(ByVal s As Stream, ByVal reader As DbDataReader, ByVal includeHeaderRow As Boolean)
        ' Make sure we have data
        If reader Is Nothing OrElse Not reader.HasRows Then
            ' Return an empty string
            Return
        End If

        Using sw As New StreamWriter(s, Encoding.Default)
            sw.AutoFlush = True

            ' Read in first record
            reader.Read()

            Dim fieldCount As Integer = reader.FieldCount

            ' If we want a header
            If includeHeaderRow Then
                ' Iterate through fields in first row, adding their names to the output
                For i As Integer = 0 To fieldCount - 1
                    ' If it's not the first field, add a comma to the output
                    If i > 0 Then sw.Write(",")
                    ' Get the field name
                    Dim fieldName As String = reader.GetName(i)
                    ' If there's a comma in the field name
                    If fieldName.IndexOf(",") > -1 Then
                        ' Add the field name enclosed in quotes, with any internal quotes escaped
                        sw.Write(String.Format("""{0}""", fieldName.Replace("""", """""")))
                    Else
                        ' Otherwise, just add the field name
                        sw.Write(fieldName)
                    End If
                Next
                sw.WriteLine()
            End If

            ' Iterate through the data, adding the field values to the output
            Do
                ' Iterate through the fields in the current row
                For i As Integer = 0 To fieldCount - 1
                    ' If it's not the first field, add a comma to the output
                    If i > 0 Then sw.Write(",")
                    ' If the field's value is not null (we have a value)
                    If Not reader.IsDBNull(i) Then
                        Dim fieldValue As String = reader.GetValue(i).ToString().Replace(vbCrLf, vbTab)
                        ' If there's a comma in the field value
                        If fieldValue.IndexOf(",") > -1 Then
                            ' Add the field value enclosed in quotes, with any internal quotes escaped
                            sw.Write(String.Format("""{0}""", fieldValue.Replace("""", """""")))
                        Else
                            ' Otherwise, just add the field value
                            sw.Write(fieldValue)
                        End If
                    End If
                Next
                sw.WriteLine()
            Loop While reader.Read()
        End Using
    End Sub

    Public Shared Sub ToCSV(ByVal s As Stream, ByVal ds As DataSet, ByVal includeHeaderRow As Boolean)
        ' Make sure we have data
        If ds Is Nothing OrElse Not ds.Tables.Count > 0 OrElse Not ds.Tables(0).Rows.Count > 0 Then
            ' Return
            Return
        End If

        Using sw As New StreamWriter(s, Encoding.Default)
            sw.AutoFlush = True

            ' Get first table
            Dim table As DataTable = ds.Tables(0)

            ' Get field count
            Dim fieldCount As Integer = table.Columns.Count

            ' If we want a header
            If includeHeaderRow Then
                ' Iterate through fields in first row, adding their names to the output
                For i As Integer = 0 To fieldCount - 1
                    ' If it's not the first field, add a comma to the output
                    If i > 0 Then sw.Write(",")
                    ' Get the field name
                    Dim fieldName As String = table.Columns(i).ColumnName
                    ' If there's a comma in the field name
                    If fieldName.IndexOf(",") > -1 Then
                        ' Add the field name enclosed in quotes, with any internal quotes escaped
                        sw.Write(String.Format("""{0}""", fieldName.Replace("""", """""")))
                    Else
                        ' Otherwise, just add the field name
                        sw.Write(fieldName)
                    End If
                Next
                sw.WriteLine()
            End If

            ' Iterate through the data, adding the field values to the output
            For Each row As DataRow In table.Rows
                ' Iterate through the fields in the current row
                For i As Integer = 0 To fieldCount - 1
                    ' If it's not the first field, add a comma to the output
                    If i > 0 Then sw.Write(",")
                    ' If the field's value is not null (we have a value)
                    If Not row.IsNull(i) Then
                        Dim fieldValue As String = row.Item(i).ToString().Replace(vbCrLf, vbTab)
                        ' If there's a comma in the field value
                        If fieldValue.IndexOf(",") > -1 Then
                            ' Add the field value enclosed in quotes, with any internal quotes escaped
                            sw.Write(String.Format("""{0}""", fieldValue.Replace("""", """""")))
                        Else
                            ' Otherwise, just add the field value
                            sw.Write(fieldValue)
                        End If
                    End If
                Next
                sw.WriteLine()
            Next
        End Using
    End Sub

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free unmanaged resources when explicitly called
            End If
            If _ds IsNot Nothing Then
                CType(_ds, IDisposable).Dispose()
                _ds = Nothing
            End If
            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

