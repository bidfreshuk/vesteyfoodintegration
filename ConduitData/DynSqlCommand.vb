Option Explicit On
Option Strict On

Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class DynSqlCommand
    Implements IDisposable
    Private m_connection As SqlConnection
    Private m_cmd As SqlCommand

    Public Property CommandText() As String
        Get
            Return m_cmd.CommandText
        End Get
        Set(ByVal value As String)
            m_cmd.CommandText = value
        End Set
    End Property

    Public Sub New()
        m_connection = New SqlConnection(Me.GetConnectionStringFromWebConfig())
        m_cmd = New SqlCommand()
        m_cmd.Connection = m_connection
        m_cmd.CommandType = CommandType.Text
    End Sub

    Public Sub New(ByVal SqlCommandText As String)
        m_connection = New SqlConnection(Me.GetConnectionStringFromWebConfig())
        m_cmd = New SqlCommand(SqlCommandText, m_connection)
        CommandText = SqlCommandText
        m_cmd.CommandType = CommandType.Text
    End Sub

    Private Function GetConnectionStringFromWebConfig() As String
        Static Dim retVal As String
        If retVal = Nothing Then

            Dim data As System.Configuration.ConnectionStringSettings
            If System.Configuration.ConfigurationManager.ConnectionStrings.Count > 1 Then
                data = ConfigurationManager.ConnectionStrings(1)
            Else
                data = ConfigurationManager.ConnectionStrings(0)
            End If

            If Not data Is Nothing Then
                retVal = data.ConnectionString
            End If
        End If
        Return retVal
    End Function

    Private Function GetConnectionStringFromWebConfig(ByVal name As String) As String
        Dim data As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings(name)
        If Not data Is Nothing Then
            Return data.ConnectionString
        End If
        Return Nothing
    End Function

    Public Function ExecuteNonQuery() As Integer
        Dim retVal As Integer = Nothing
        Try
            If m_connection.State = ConnectionState.Closed Then
                m_connection.Open()
            End If
            retVal = m_cmd.ExecuteNonQuery()
        Finally
            If m_connection.State = ConnectionState.Open Then
                m_connection.Close()
            End If
        End Try
        Return retVal
    End Function

    Public Function ExecuteReader(ByVal behavior As CommandBehavior) As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        Try
            If m_connection.State = ConnectionState.Closed Then
                m_connection.Open()
            End If
            reader = m_cmd.ExecuteReader(behavior)
        Catch
            If Not reader Is Nothing Then
                If Not reader.IsClosed Then reader.Close()
            End If
            Throw
        End Try
        Return reader
    End Function

    Public Function ExecuteReader() As SqlDataReader
        Return Me.ExecuteReader(CommandBehavior.CloseConnection)
    End Function


    Public Function ExecuteSimpleDataReader() As Conduit.Data.SimpleDataReader
        Dim reader As SqlDataReader = Nothing
        Try
            If m_connection.State = ConnectionState.Closed Then
                m_connection.Open()
            End If
            reader = m_cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Return New Conduit.Data.SimpleDataReader(reader)
        Finally
            If Not reader Is Nothing Then
                If Not reader.IsClosed Then reader.Close()
            End If
        End Try
    End Function

    Public Function ExecuteDataSet(ByVal name As String) As DataSet
        Dim dataSet As New DataSet(name)
        Dim dataAdapter As New SqlDataAdapter(m_cmd)
        Try
            If m_connection.State = ConnectionState.Closed Then
                m_connection.Open()
            End If
            dataAdapter.Fill(dataSet)
        Finally
            If m_connection.State = ConnectionState.Open Then
                m_connection.Close()
            End If
        End Try
        Return dataSet
    End Function

    Public Function ExecuteDataSet() As DataSet
        Dim dataSet As New DataSet()
        Dim dataAdapter As New SqlDataAdapter(m_cmd)
        Try
            If m_connection.State = ConnectionState.Closed Then
                m_connection.Open()
            End If
            dataAdapter.Fill(dataSet)
        Finally
            If m_connection.State = ConnectionState.Open Then
                m_connection.Close()
            End If
        End Try
        Return dataSet
    End Function

    Public Sub ClearParameters()
        m_cmd.Parameters.Clear()
    End Sub

    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder
        sb.Append(m_cmd.CommandText)
        If m_cmd.Parameters.Count > 0 Then
            For Each param As SqlParameter In m_cmd.Parameters
                sb.Append(" ")
                Select Case param.SqlDbType
                    Case SqlDbType.Char, SqlDbType.NChar, SqlDbType.NText, SqlDbType.NVarChar, _
                      SqlDbType.Text, SqlDbType.UniqueIdentifier, SqlDbType.VarChar, SqlDbType.Xml
                        sb.AppendFormat("'{0}'", param.SqlValue.ToString())
                    Case SqlDbType.Binary, SqlDbType.Image, SqlDbType.VarBinary
                        sb.Append("[BINARY]")
                    Case Else
                        sb.Append(param.SqlValue.ToString())
                End Select
                sb.Append(",")
            Next
            sb.Remove(sb.Length - 1, 1)
        End If
        Return sb.ToString()
    End Function

    Public Sub CloseConnection()
        If m_connection IsNot Nothing Then
            If m_connection.State <> ConnectionState.Closed Then
                m_connection.Close()
            End If
        End If
    End Sub

    Public Sub AddParameter(ByVal paramName As String, ByVal value As Object)
        If value Is Nothing Then
            m_cmd.Parameters.AddWithValue(paramName, DBNull.Value)
        Else
            m_cmd.Parameters.AddWithValue(paramName, value)
        End If
    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        m_connection.Dispose()
    End Sub

End Class
