﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conduit.Data;

namespace vesteyconsole
{
    public static class DataAccess
    {

        public static Int64 insertFilelog(string filename,string location,string status,out string consolidatedOrder )
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[FileLogInsert]"))
            {
                sp.AddParameter<string>("@filename", filename);
                sp.AddParameter<string>("@location", location);
                sp.AddParameter<string>("@status", status);
               
                sp.AddOutputParameter("@new_identity", SqlDbType.BigInt);
                sp.AddOutputParameter("@ConsolidatedOrderID",SqlDbType.NVarChar,36);
                sp.ExecuteNonQuery();
                consolidatedOrder = sp.GetOutputParameter<string>("@ConsolidatedOrderID");
                return sp.GetOutputParameter<Int64>("@new_identity"); 

            }
        }

        public static int insertFileContents(Int64 filelogid, int rownumber, string linecontents)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[FileContentsInsert]"))
            {
                sp.AddParameter<Int64>("@FileLogID", filelogid);
                sp.AddParameter<int>("@RowNumber",rownumber);
                sp.AddParameter<string>("@LineContents", linecontents);
                return sp.ExecuteNonQuery();
            }
        }

        public static int generateOrderDetails(Int64 filelogid)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[GenerateOrderDetails]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                return sp.ExecuteNonQuery();
            }
        }

        public static List<OraOrder> getOrderHeaderForOracle(Int64 filelogid)
        {
            List<OraOrder> orders = new List<OraOrder>();

            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[OrderHeaderDetails]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                using (SimpleDataReader rd = sp.ExecuteSimpleDataReader())
                {
                    while (rd.Read())
                    {
                        OraOrder order = new OraOrder();
                        order.SourceTransactionIdentifier = rd.GetString("OrderID");
                        order.SourceTransactionSystem = "OPS";
                        order.SourceTransactionNumber = rd.GetString("OrderID");
                        order.BuyingPartyId = rd.GetString("BuyingPartyID");
                        order.BuyingPartyName = rd.GetString("BuyingPartyName");
                        order.TransactionalCurrencyCode = "GBP";
                        order.CustomerPONumber = rd.GetString("CustomerPONumber");
                        order.TransactionOn = rd.GetValue<DateTime>("TransactionOn").ToString("yyyy-MM-ddTHH:mm:ss.0000");
                        order.RequestingBusinessUnitIdentifier = rd.GetString("RequestingBusinessUnitIdentifier");
                        order.FreezePriceFlag = "false";
                        order.FreezeShippingChargeFlag = "false";
                        order.FreezeTaxFlag = "false";
                        order.ShipToPartyName = rd.GetString("ShipToPartyName");
                        order.BillToCustomerName = rd.GetString("BillToCustomerName");
                        order.BillToAccountName = rd.GetString("BillToAccountName");
                        order.HeaderNotes = rd.GetString("HeaderNotes");
                        order.OrderType = rd.GetString("OrderType");
                        order.ShipmentPriorityCode = rd.GetString("ShipmentPriorityCode");
                        order.PaymentTerm = rd.GetString("PaymentTerm");
                        order.BillToCustomerIdentifier = rd.GetString("CustAccountID");
                        order.Depot = rd.GetString("Depot");
                        order.OrderNo = rd.GetString("OrderNo");
                        orders.Add(order);
                    }
                }

            }

                return orders;  
        }


        public static List<OraOrderLine> getLines(Int64 filelogid,string depot,string order)
        {
            List<OraOrderLine> items = new List<OraOrderLine>();
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[OrderLineDetails]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                sp.AddParameter<string>("@Depot", depot);
                sp.AddParameter<string>("@Order", order);

                using (SimpleDataReader rd = sp.ExecuteSimpleDataReader())
                {
                    while (rd.Read())
                    {
                        OraOrderLine line = new OraOrderLine();
                        line.SourceTransactionLineIdentifier = rd.GetString("SourceTransactionLineIdentifier");
                        line.SourceTransactionScheduleIdentifier = rd.GetString("SourceTransactionScheduleIdentifier");
                        line.SourceTransactionLineNumber = rd.GetString("SourceTransactionLineNumber");
                        line.SourceTransactionScheduleNumber = rd.GetString("SourceTransactionScheduleNumber");
                        line.ProductNumber = rd.GetString("ProductNumber");
                        line.OrderedQuantity = rd.GetValue<decimal>("OrderedQuantity").ToString("#####0.00");
                        line.TaxExempt = rd.GetString("TaxExempt");
                        line.OrderedUOM = rd.GetString("OrderedUOM");
                        line.RequestingBusinessUnitIdentifier = rd.GetString("RequestingBusinessUnitIdentifier");
                        line.RequestedShipDate = rd.GetValue<DateTime>("RequestedShipDate").ToString("yyyy-MM-ddTHH:mm:ss.0000");
                        line.ShipmentPriorityCode = rd.GetString("ShipmentPriorityCode");
                        line.TransactionCategoryCode = rd.GetString("TransactionCategoryCode");
                        line.RequestedFulfillmentOrganizationCode = rd.GetString("RequestedFulfillmentOrganizationCode");
                        line.InventoryOrganization = rd.GetString("InventoryOrganization");
                        line.BillToContactPersonName = rd.GetString("BillToContactPersonName");
                        line.ShipToPartyIdentifier = rd.GetString("ShipToPartyIdentifier");
                        line.ShipToPartySiteIdentifier = rd.GetString("ShipToPartySiteIdentifier");
                        line.BillToCustomerIdentifier = rd.GetString("BillToCustomerIdentifier");
                        line.BillToAccountSiteUseIdentifier = rd.GetString("BillToAccountSiteUseIdentifier");
                        line.PartialShipAllowedFlag = rd.GetString("PartialShipAllowedFlag");
                        line.ShippingInstructions = rd.GetString("ShippingInstructions");
                        items.Add(line);
                    }
                }
            }

            return items;
        }

        public static int updateOrderHeaderWithOracleOrderNo(Int64 filelogid, string depot, string order,string OracleOrder)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[OrderHeaderUpdateOracleOrderNo]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                sp.AddParameter<string>("@Depot", depot);
                sp.AddParameter<string>("@Order", order);
                sp.AddParameter<string>("@OracleOrder", OracleOrder);
                return sp.ExecuteNonQuery();  
            }
        }

        public static string getResponseFileContents(Int64 filelogid, string consolidatedOrder)
        {

            string filecontents = string.Empty;
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[GenerateResponseFileDetails]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                sp.AddParameter<string>("@ConsolidatedOrderNo", consolidatedOrder);
                using (SimpleDataReader rd = sp.ExecuteSimpleDataReader())
                {
                    while (rd.Read())
                    {
                        string linecontent = rd.GetString("LineContents", ""); 
                        if(!String.IsNullOrEmpty(linecontent))
                        {
                            filecontents += linecontent + Environment.NewLine;
                        }
                    }
                }


            }


           return filecontents;  

        }


        public static int updateFileLog(Int64 filelogid, string ResponseFileName, string ResponseFileLocation)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[UpdateFileLogStatus]"))
            {
                sp.AddParameter<Int64>("@fileid", filelogid);
                sp.AddParameter<string>("@ResponseFileName", ResponseFileName);
                sp.AddParameter<string>("@ResponseFileLocation", ResponseFileLocation); 
                return sp.ExecuteNonQuery();
            }
        }

        public static int emailAuditOrderFile(Int64 filelogid)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[ExtractVesteyReportEmail]"))
            {
                sp.AddParameter<Int64>("@LogId", filelogid);
                return sp.ExecuteNonQuery();
            }
        }

        public static int emailOrderException(Int64 filelogid)
        {
            using (StoredProcedure sp = new StoredProcedure("[VESTEY].[ExtractVesteyNoOracleOrderNotifications]"))
            {
                sp.AddParameter<Int64>("@LogId", filelogid);
                return sp.ExecuteNonQuery();
            }
        }

    }
}
