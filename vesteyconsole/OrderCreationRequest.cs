﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vesteyconsole
{
    public class OrderCreationRequest
    {
        public string batchName { get; set; }
        public List<OraOrder> Orders { get; set; }
    }

    public class OraOrder
    {
        public string SourceTransactionIdentifier { get; set; }
        public string SourceTransactionSystem { get; set; }
        public string SourceTransactionNumber { get; set; }
        public string BuyingPartyId { get; set; }
        public string BuyingPartyName { get; set; }
        public string TransactionalCurrencyCode { get; set; }
        public string CustomerPONumber { get; set; }
        public string TransactionOn { get; set; }
        public string RequestingBusinessUnitIdentifier { get; set; }
        public string FreezePriceFlag { get; set; }
        public string FreezeShippingChargeFlag { get; set; }
        public string FreezeTaxFlag { get; set; }
        public string ShipToPartyName { get; set; }
        public string BillToCustomerName { get; set; }
        public string BillToAccountName { get; set; }
        public string HeaderNotes { get; set; }
        public string OrderType { get; set; }
        public string ShipmentPriorityCode { get; set; }
        public string PaymentTerm { get; set; }
        public string BillToCustomerIdentifier { get; set; }
        public string Depot { get; set; }
        public string OrderNo { get; set; }
        public List<OraOrderLine> Oraorderlines { get; set; }
    }

    public class OraOrderLine
    {
        public string SourceTransactionLineIdentifier { get; set; }
        public string SourceTransactionScheduleIdentifier { get; set; }
        public string SourceTransactionLineNumber { get; set; }
        public string SourceTransactionScheduleNumber { get; set; }
        public string ProductNumber { get; set; }
        public string OrderedQuantity { get; set; }
        public string TaxExempt { get; set; }
        public string OrderedUOM { get; set; }
        public string RequestingBusinessUnitIdentifier { get; set; }
        public string RequestedShipDate { get; set; }
        public string ShipmentPriorityCode { get; set; }
        public string TransactionCategoryCode { get; set; }
        public string RequestedFulfillmentOrganizationCode { get; set; }
        public string InventoryOrganization { get; set; }
        public string BillToContactPersonName { get; set; }
        public string PaymentTerms { get; set; }
        public string ShipToPartyIdentifier { get; set; }
        public string ShipToPartySiteIdentifier { get; set; }
        public string BillToCustomerIdentifier { get; set; }
        public string BillToAccountSiteUseIdentifier { get; set; }
        public string PartialShipAllowedFlag { get; set; }
        public string ShippingInstructions { get; set; }
    }
}
