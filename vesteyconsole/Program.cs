﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conduit.ConsoleBase;
using Conduit.Data;
using System.IO;
using System.Security;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace vesteyconsole
{
    public abstract class Program : MainBase
    {

        private static string PickupOrderFileLocation;
        private static string ProcessedOrderFileLocation;
        private static string ErrorOrderFileLocation;
        private static string Orderfilenamestartwith;
        private static string EmailCopyOfOrderTo;
        private static string OrderFileCopySubject;
        private static string NotificationEmail;
        private static string StartProcessingStatus;
        private static string OracleWebserviceURL;
        private static string OracleUsername;
        private static string OraclePassword;
        private static string ResponseFileLocation;
        private static int NumberOfTries;
        static int Main()
        {

            return InitAndTryCatchWrapMainProcess(MainProcess);
        }

      
        protected static void MainProcess()
        {
            Initialise();
        }


        private static void Initialise()
        {
            _log.Write("");
            _log.Write("***********Start Process****************************");
            PickupOrderFileLocation = GetExpectedAppSetting("PickupOrderFileLocation");
            ProcessedOrderFileLocation = GetExpectedAppSetting("ProcessedOrderFileLocation");
            ErrorOrderFileLocation = GetExpectedAppSetting("ErrorOrderFileLocation");
            Orderfilenamestartwith = GetExpectedAppSetting("Orderfilenamestartwith");
            EmailCopyOfOrderTo = GetExpectedAppSetting("EmailCopyOfOrderTo");
            OrderFileCopySubject = GetExpectedAppSetting("OrderFileCopySubject");
            NotificationEmail = GetExpectedAppSetting("NotificationEmail");
            StartProcessingStatus = GetExpectedAppSetting("StartProcessingStatus"); 
            OracleWebserviceURL = GetExpectedAppSetting("OracleWebserviceURL");
            OracleUsername = GetExpectedAppSetting("OracleUsername");
            OraclePassword = GetExpectedAppSetting("OraclePassword");
            ResponseFileLocation = GetExpectedAppSetting("ResponseFileLocation");
            string numtr = GetExpectedAppSetting("NumberOfTries");
            int i = 0;
            int.TryParse(numtr, out i); 
            if(i > 0)
            {
                NumberOfTries = i;
            }
            else
            {
                NumberOfTries = 3; 
            }

            _log.Write("Reading Directory for files " + PickupOrderFileLocation);
            DirectoryInfo d = new DirectoryInfo(PickupOrderFileLocation);


            foreach (var file in d.GetFiles("DBCD*_OO"))
            {
                _log.Write("");
                
                _log.Write(file.FullName);

                EmailCopyOftheFile(file.FullName);

                string consolidatedOrderID = ""; 
                //Int64 logid = LogFileToDB(file.Name, file.FullName, file.Name.Replace("_OO", ""));
                Int64 logid = LogFileToDB(file.Name, file.FullName,out consolidatedOrderID);

                _log.Write("File Log ID: " + logid.ToString());
                _log.Write("ConsolidatedOrderID: " + consolidatedOrderID);  

                storefilecontents(logid, file.FullName);
                GenerateOrdersInDBByFileID(logid);

                _log.Write("Get data from DB to submit to Oracle");


                List<OraOrder> orders = DataAccess.getOrderHeaderForOracle(logid); 
                if(orders != null && orders.Count > 0)
                {
                    _log.Write(orders.Count.ToString() +   " orders got from DB");
                    foreach (var o in orders)
                    {
                        _log.Write("For each header populate lines");
                        _log.Write(o.OrderNo);
                        _log.Write(o.Depot);

                        o.Oraorderlines = DataAccess.getLines(logid, o.Depot, o.OrderNo);

                    }

                    generateOrdersInOracle(orders, logid);

                }
                else
                {
                    _log.Write("Zero orders got from DB");  
                }

                string responseFileContent = getResponseFileContents(logid, consolidatedOrderID);
                if (!string.IsNullOrEmpty(responseFileContent))
                {
                    string responsefilename = file.Name.Replace("_OO", "_OR");
                    _log.Write("Got file response text from DB");
                    _log.Write("Writing response to " + ResponseFileLocation + responsefilename);
                    File.WriteAllText(ResponseFileLocation + responsefilename, responseFileContent);

                    _log.Write("Update file log database table");
                    DataAccess.updateFileLog(logid, responsefilename, ResponseFileLocation);
                }
                _log.Write("Email file Audit report to depot");
                DataAccess.emailAuditOrderFile(logid);

                _log.Write("Email Order Exceptions");
                DataAccess.emailOrderException(logid);

                

            }

            //Console.ReadLine();

            _log.Write("Move Files to Processed folder: " + ProcessedOrderFileLocation );
           // File.Move(file.FullName, ProcessedOrderFileLocation + file.Name);


            if (!Directory.Exists(ProcessedOrderFileLocation))
            {
                Directory.CreateDirectory(ProcessedOrderFileLocation);
            }

            string[] sourcefiles = Directory.GetFiles(PickupOrderFileLocation);

            foreach (string sourcefile in sourcefiles)
            {
                string fileName = Path.GetFileName(sourcefile);
                string destFile = Path.Combine(ProcessedOrderFileLocation, fileName);
                _log.Write("Move " + sourcefile + " to " + destFile);  
                if(File.Exists(destFile))
                {
                    _log.Write("Dest File Exists so deleting " + destFile); 
                    File.Delete(destFile);  
                }
                File.Move(sourcefile, destFile);
            }

        }



        private static void EmailCopyOftheFile(string filename)
        {

            if(File.Exists(filename))
            {
                _log.Write("Emailing File to " + EmailCopyOfOrderTo); 
              _email.SendWithAttachment(OrderFileCopySubject, OrderFileCopySubject, EmailCopyOfOrderTo, NotificationEmail, new string[] { filename });  
            }

        }

        private static Int64 LogFileToDB(string filename,string filelocation,out string consolidatedorderid)
        {
            _log.Write("log the file to the database");   
            return DataAccess.insertFilelog(filename, filelocation, StartProcessingStatus, out consolidatedorderid); 
        }

        private static void storefilecontents(Int64 fileogid, string fileName)
        {
            _log.Write("log the file contents to the database");
            string[] filecontents = File.ReadAllLines(fileName);

            if(filecontents != null)
            {
                int i = 1;  
                foreach (var item in filecontents)
                {
                    DataAccess.insertFileContents(fileogid, i, item);
                    i++; 
                }
            }
        }

        private static void GenerateOrdersInDBByFileID(Int64 fileogid)
        {
            _log.Write("genrate the order header and line records in DB");
            DataAccess.generateOrderDetails(fileogid); 
        }

        private static void generateOrdersInOracle(List<OraOrder> orders, Int64 fileogid)
        {
            _log.Write("Procedure to submit orders to Oracle");
            _log.Write("Oracle URL: " + OracleWebserviceURL);

            _log.Write("Number of orders to submit: {0}", orders.Count);

            foreach (var h in orders)
            {
                int numberOfTries = 0;
                string text = string.Empty;

                _log.Write("");
                _log.Write("Working on Order {0}", h.SourceTransactionIdentifier);

                text = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:typ=""http://xmlns.oracle.com/apps/scm/fom/importOrders/orderImportService/types/"" xmlns:ord=""http://xmlns.oracle.com/apps/scm/fom/importOrders/orderImportService/"" xmlns:mod=""http://xmlns.oracle.com/apps/scm/doo/processOrder/model/"" xmlns:ns12=""http://xmlns.oracle.com/apps/scm/doo/processOrder/flex/headerCategories/"" xmlns:ns22=""http://xmlns.oracle.com/apps/scm/doo/processOrder/flex/headerContextsB/"" xmlns:xsi=""xsi"">";
                text += @"<soapenv:Header />";
                text += @"<soapenv:Body>";
                text += @"<typ:createOrders>";
                text += @"<typ:request>";
                text += @"<ord:BatchName>" + SecurityElement.Escape(h.SourceTransactionIdentifier) + "</ord:BatchName>";


                text += @"<ord:Order>";
                text += @"<ord:SourceTransactionIdentifier>" + SecurityElement.Escape(h.SourceTransactionIdentifier) + "</ord:SourceTransactionIdentifier>";
                text += @"<ord:SourceTransactionSystem>" + SecurityElement.Escape(h.SourceTransactionSystem) + "</ord:SourceTransactionSystem>";
                text += @"<ord:SourceTransactionNumber>" + SecurityElement.Escape(h.SourceTransactionNumber) + "</ord:SourceTransactionNumber>";
                text += @"<ord:BuyingPartyId>" + SecurityElement.Escape(h.BuyingPartyId) + "</ord:BuyingPartyId>";
                text += @"<ord:BuyingPartyName>" + SecurityElement.Escape(h.BuyingPartyName) + "</ord:BuyingPartyName>";
                text += @"<ord:TransactionalCurrencyCode>" + SecurityElement.Escape(h.TransactionalCurrencyCode) + "</ord:TransactionalCurrencyCode>";
                text += @"<ord:CustomerPONumber>" + SecurityElement.Escape(h.CustomerPONumber) + "</ord:CustomerPONumber>";
                text += @"<ord:TransactionOn>" + SecurityElement.Escape(h.TransactionOn) + "</ord:TransactionOn>";
                text += @"<ord:RequestingBusinessUnitIdentifier>" + SecurityElement.Escape(h.RequestingBusinessUnitIdentifier) + "</ord:RequestingBusinessUnitIdentifier>";
                text += @"<ord:FreezePriceFlag>" + SecurityElement.Escape(h.FreezePriceFlag) + "</ord:FreezePriceFlag>";
                text += @"<ord:FreezeShippingChargeFlag>" + SecurityElement.Escape(h.FreezeShippingChargeFlag) + "</ord:FreezeShippingChargeFlag>";
                text += @"<ord:FreezeTaxFlag>" + SecurityElement.Escape(h.FreezeTaxFlag) + "</ord:FreezeTaxFlag>";
                text += @"<ord:ShipToPartyIdentifier>" + SecurityElement.Escape(h.BuyingPartyId) + "</ord:ShipToPartyIdentifier>";
                text += @"<ord:BillToCustomerIdentifier>" + SecurityElement.Escape(h.BillToCustomerIdentifier) + "</ord:BillToCustomerIdentifier>";
                if(h.Oraorderlines != null && h.Oraorderlines.Count > 0)
                {
                    text += @"<ord:ShipToPartySiteIdentifier>" + SecurityElement.Escape(h.Oraorderlines[0].ShipToPartySiteIdentifier) + "</ord:ShipToPartySiteIdentifier>";
                }
                foreach (var l in h.Oraorderlines)
                {
                    text += @"<ord:Line>";
                    text += @"<ord:SourceTransactionLineIdentifier>" + SecurityElement.Escape(l.SourceTransactionLineIdentifier) + "</ord:SourceTransactionLineIdentifier>";
                    text += @"<ord:SourceTransactionScheduleIdentifier>" + SecurityElement.Escape(l.SourceTransactionScheduleNumber) + "</ord:SourceTransactionScheduleIdentifier>";
                    text += @"<ord:SourceTransactionLineNumber>" + SecurityElement.Escape(l.SourceTransactionLineNumber) + "</ord:SourceTransactionLineNumber>";
                    text += @"<ord:SourceTransactionScheduleNumber>" + SecurityElement.Escape(l.SourceTransactionScheduleNumber) + "</ord:SourceTransactionScheduleNumber>";
                    text += @"<ord:ProductNumber>" + SecurityElement.Escape(l.ProductNumber) + "</ord:ProductNumber>";
                    text += @"<ord:OrderedQuantity>" + SecurityElement.Escape(l.OrderedQuantity) + "</ord:OrderedQuantity>";
                    text += @"<ord:OrderedUOMCode>" + SecurityElement.Escape(l.OrderedUOM) + "</ord:OrderedUOMCode>";
                    text += @"<ord:RequestingBusinessUnitIdentifier>" + SecurityElement.Escape(l.RequestingBusinessUnitIdentifier) + "</ord:RequestingBusinessUnitIdentifier>";
                    text += @"<ord:RequestedShipDate>" + SecurityElement.Escape(l.RequestedShipDate) + "</ord:RequestedShipDate>";
                    text += @"<ord:TransactionCategoryCode>" + SecurityElement.Escape(l.TransactionCategoryCode) + "</ord:TransactionCategoryCode>";
                    text += @"<ord:RequestedFulfillmentOrganizationCode>" + SecurityElement.Escape(l.RequestedFulfillmentOrganizationCode) + "</ord:RequestedFulfillmentOrganizationCode>";
                    text += @"<ord:InventoryOrganization>" + SecurityElement.Escape(l.InventoryOrganization) + "</ord:InventoryOrganization>";
                    text += @"<ord:BillToContactPersonName>" + SecurityElement.Escape(l.BillToContactPersonName) + "</ord:BillToContactPersonName>";
                    if (!string.IsNullOrEmpty(h.PaymentTerm))
                    {
                        text += @"<ord:PaymentTerms>" + SecurityElement.Escape(h.PaymentTerm) + "</ord:PaymentTerms>";
                    }
                    if (!string.IsNullOrEmpty(h.ShipmentPriorityCode))
                    {
                        text += @"<ord:ShipmentPriorityCode>" + SecurityElement.Escape(h.ShipmentPriorityCode) + "</ord:ShipmentPriorityCode>";
                    }
                    text += @"<ord:ShipToPartyIdentifier>" + SecurityElement.Escape(l.ShipToPartyIdentifier) + "</ord:ShipToPartyIdentifier>";
                    text += @"<ord:ShipToPartySiteIdentifier>" + SecurityElement.Escape(l.ShipToPartySiteIdentifier) + "</ord:ShipToPartySiteIdentifier>";
                    text += @"<ord:BillToCustomerIdentifier>" + SecurityElement.Escape(l.BillToCustomerIdentifier) + "</ord:BillToCustomerIdentifier>";
                    text += @"<ord:BillToAccountSiteUseIdentifier>" + SecurityElement.Escape(l.BillToAccountSiteUseIdentifier) + "</ord:BillToAccountSiteUseIdentifier>";
                    text += @"<ord:PartialShipAllowedFlag>" + SecurityElement.Escape(l.PartialShipAllowedFlag) + "</ord:PartialShipAllowedFlag>";
                    if (!string.IsNullOrEmpty(l.ShippingInstructions))
                    {
                        text += @"<ord:ShippingInstructions>" + SecurityElement.Escape(l.ShippingInstructions) + "</ord:ShippingInstructions>";
                    }
                    text += @"</ord:Line>";
                }


                text += @"<ord:AdditionalHeaderInformationCategories xsi:type=""ns12:j_HeaderEffDooHeadersAddInfoprivate"" xmlns:ns3=""http://xmlns.oracle.com/apps/scm/doo/processOrder/service/"" xmlns:ns8=""http://xmlns.oracle.com/apps/scm/doo/processOrder/model/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">";
                text += @"<ns8:Category>" + "DOO_HEADERS_ADD_INFO" + "</ns8:Category>";
                text += @"<ns12:HeaderEffBHeader_5FNotesprivateVO>";
                text += @"<ns8:ContextCode>" + "Header_Notes" + "</ns8:ContextCode>";
                text += @"<ns22:headerNotes>" + SecurityElement.Escape(h.HeaderNotes) + "</ns22:headerNotes>";
                text += @"</ns12:HeaderEffBHeader_5FNotesprivateVO>";
                text += @"</ord:AdditionalHeaderInformationCategories>";
                text += @"<ord:OrderPreferences>";
                text += @"<ord:CreateCustomerInformationFlag>" + "false" + "</ord:CreateCustomerInformationFlag>";
                text += @"<ord:SubmitFlag>" + "true" + "</ord:SubmitFlag>";
                text += @"</ord:OrderPreferences>";
                text += @"</ord:Order>";


                text += @"</typ:request>";
                text += @"</typ:createOrders>";
                text += @"</soapenv:Body>";
                text += @"</soapenv:Envelope>";

                _log.Write("");
                _log.Write(text);

                while (numberOfTries < NumberOfTries )
                {

                    _log.Write("Number of tries: " + numberOfTries.ToString());   
                    var byteArray = Encoding.ASCII.GetBytes(OracleUsername + ":" + OraclePassword);
                    text.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", string.Empty);
                    byte[] bytes = Encoding.UTF8.GetBytes(text);
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(OracleWebserviceURL);
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));
                    httpWebRequest.Accept = "text/xml";
                    httpWebRequest.ContentType = "text/xml";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentLength = (long)bytes.Length;
                    httpWebRequest.Timeout = 1000000;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    string x = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                    UTF8Encoding uTF8Encoding = new UTF8Encoding();
                    byte[] bytes2 = uTF8Encoding.GetBytes(x);
                    MemoryStream input = new MemoryStream(bytes2);
                    XmlTextReader reader = new XmlTextReader(input);
                    XDocument xDoc = XDocument.Load(reader);
                    //var result = xDoc.Root.Value;

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(xDoc.ToString());

                    XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);

                    xmlnsManager.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                    xmlnsManager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    xmlnsManager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                    xmlnsManager.AddNamespace("si", "http://xmlns.oracle.com/apps/scm/fom/importOrders/orderImportService/");

                    _log.Write("");
                    _log.Write(xDoc.ToString());

                    XmlNode node = xmlDoc.SelectSingleNode("//si:ReturnStatus", xmlnsManager);
                    string ReturnStatus = node.InnerText;

                    node = xmlDoc.SelectSingleNode("//si:MessageText", xmlnsManager);
                    string MessageText = node.InnerText;

                    node = xmlDoc.SelectSingleNode("//si:MessageName", xmlnsManager);
                    string MessageName = node.InnerText;

                    node = xmlDoc.SelectSingleNode("//si:OrderNumber", xmlnsManager);
                    string OracleOrderNo = node.InnerText;

                    _log.Write("");

                    _log.Write("ReturnStatus: {0}", ReturnStatus);
                    _log.Write("MessageText: {0}", MessageText);
                    _log.Write("MessageName: {0}", MessageName);
                    _log.Write("OracleOrderNO: {0}", OracleOrderNo);

                    

                    if (ReturnStatus == "SUCCESS")
                    {
                        _log.Write("Updating Order Status");
                        //DataAccess.UpdateOrderStatus(h.SourceTransactionIdentifier);
                        //if (NotifyForComments)
                        //{
                        //    _log.Write("Notifying for Comments");
                        //    DataAccess.EmailNotificationForOrderHeaderComments(h.SourceTransactionIdentifier);
                        //}

                        DataAccess.updateOrderHeaderWithOracleOrderNo(fileogid, h.Depot, h.OrderNo, OracleOrderNo);  
                        break; 
                    }
                    else
                    {
                        if (!MessageText.StartsWith("JBO-29000"))
                        {
                            //_log.Write("Emailing for Error and update staus of orders to U");
                            //DataAccess.UpdateOrderStatus(h.SourceTransactionIdentifier, "U");
                            //DataAccess.EmailNotificationForOrderError(h.SourceTransactionIdentifier, MessageText);
                            numberOfTries++;
                        }
                        else
                        {
                            _log.Write("Error JBO-29000 so leave status as is to resubmit next cycle");
                            numberOfTries++; 
                        }
                    }
                }


            }

        }
        
        private static string getResponseFileContents(Int64 filelogid, string consolidatedorderid)
        {
            _log.Write("get response file contents from DB");
            return DataAccess.getResponseFileContents(filelogid, consolidatedorderid);  
        }


    }
}
